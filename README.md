### Welcome to my `2021 Advent of Code` solutions made in `Rust`! 26☆

[https://adventofcode.com/](https://adventofcode.com/)

#### Table of Contents

| `Day` | `Part` | `Solution` |
|-----|------|----------|
| 1   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day1p1/mod.rs)      |
| 1   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day1p2/mod.rs)      |
| 2   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day2p1/mod.rs)      |
| 2   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day2p2/mod.rs)      |
| 3   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day3p1/mod.rs)      |
| 3   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day3p2/mod.rs)      |
| 4   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day4p1/mod.rs)      |
| 4   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day4p2/mod.rs)      |
| 5   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day5p1/mod.rs)      |
| 5   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day5p2/mod.rs)      |
| 6   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day6/mod.rs)        |
| 6   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day6/mod.rs)        |
| 7   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day7p1/mod.rs)      |
| 7   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day7p2/mod.rs)      |
| 8   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day8p1/mod.rs)      |
| 8   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day8p2/mod.rs)      |
| 9   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day9p1/mod.rs)      |
| 9   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day9p2/mod.rs)      |
| 10   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day10p1/mod.rs)      |
| 10   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day10p2/mod.rs)      |
| 11   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day11p1/mod.rs)      |
| 11   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day11p2/mod.rs)      |
| 12   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day12p1/mod.rs)      |
| 12   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day12p2/mod.rs)      |
| 13   | 1    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day13p1/mod.rs)      |
| 13   | 2    |    [code](https://gitlab.com/pcanilho/advent-of-code-2021/-/blob/master/src/day13p2/mod.rs)      |