use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

pub(crate) fn run(path: &str, days: i32) -> Result<i64, Box<dyn Error>> {
    let file = BufReader::new(File::open(path)?);

    let mut generations: HashMap<i32, f64> = HashMap::new();
    for lw in file.lines() {
        let line = lw?;

        for v in line.split(",") {
            let timer = v.parse::<i32>()?;
            update_generations(generations.borrow_mut(), timer, 1.0)
        }

        simulate(generations.borrow_mut(), days);

        let mut fish_count = 0;
        for (_, count) in generations.iter() {
            fish_count += (*count) as i64
        }

        return Ok(fish_count);
    }
    Ok(0)
}

fn simulate(generations: &mut HashMap<i32, f64>, mut days: i32) {
    while days >= 0 {
        days -= 1;

        let mut next_generation: HashMap<i32, f64> = HashMap::new();
        for (timer, count) in generations.iter() {
            let new_timer = timer - 1;
            if new_timer < 0 {
                update_generations(next_generation.borrow_mut(), 6, *count);
                update_generations(next_generation.borrow_mut(), 8, *count);
            } else {
                update_generations(next_generation.borrow_mut(), new_timer, *count);
            }
        }
        *generations = next_generation;
    }
}

fn update_generations(generations: &mut HashMap<i32, f64>, timer: i32, members: f64) {
    *generations.entry(timer).or_insert(0.0) += members;
}
