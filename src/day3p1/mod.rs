use std::collections::HashMap;
use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;
use std::iter::FromIterator;

pub(crate) fn run(path: &str) -> Result<isize, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let mut result: HashMap<usize, Vec<char>> = HashMap::new();
    for lw in f.lines() {
        let line = lw.unwrap();
        for (i, c) in line.chars().enumerate() {
            let entry = result.get_mut(&i);
            if entry.is_none() {
                result.insert(i, vec![c]);
            } else {
                entry.unwrap().push(c);
            }
        }
    }


    let mut gamma_bits = Vec::with_capacity(5);
    let mut epsilon_bits = Vec::new();
    let mut sorted: Vec<_> = result.iter().collect();
    sorted.sort_by_key(|x| x.0);
    for (k, v) in sorted {
        let mut s_hash = HashMap::new();
        for c in v {
            *s_hash.entry((k, c)).or_insert(0) += 1;
        }
        if s_hash.get(&(k, &'0')).unwrap() >= s_hash.get(&(k, &'1')).unwrap() {
            gamma_bits.insert(*k, '0');
            epsilon_bits.insert(*k, '1');
        } else {
            gamma_bits.insert(*k, '1');
            epsilon_bits.insert(*k, '0');
        }
    }
    let gamma_decimal = isize::from_str_radix(String::from_iter(gamma_bits).as_str(), 2).unwrap();
    let epsilon_decimal = isize::from_str_radix(String::from_iter(epsilon_bits).as_str(), 2).unwrap();

    Ok(gamma_decimal*epsilon_decimal)
}
