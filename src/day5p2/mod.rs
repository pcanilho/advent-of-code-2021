use std::borrow::{Borrow, BorrowMut};
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;
use std::process::exit;

pub(crate) fn run(path: &str) -> Result<usize, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let mut acc = HashMap::new();
    for lw in f.lines() {
        let raw = lw.unwrap();
        let line = raw.split(" -> ").collect::<Vec<&str>>();

        let (l, r) = (line.first().unwrap(), line.last().unwrap());
        let l_raw = l.split(",").collect::<Vec<&str>>();
        let r_raw = r.split(",").collect::<Vec<&str>>();

        let lc = l_raw.first().unwrap().parse::<i32>().unwrap();
        let lr = l_raw.last().unwrap().parse::<i32>().unwrap();

        let rc = r_raw.first().unwrap().parse::<i32>().unwrap();
        let rr = r_raw.last().unwrap().parse::<i32>().unwrap();

        let (mut lc_bounds, rc_bounds) = if lc < rc { (lc, rc) } else { (rc, lc) };
        let (mut lr_bounds, r_bounds) = if lr < rr { (lr, rr) } else { (rr, lr) };

        if lc != rc && lr != rr {
            get_all_diag_points((lc, lr), (rc, rr), acc.borrow_mut());
        } else {
            if lc_bounds != rc_bounds {
                for col in lc_bounds..=rc_bounds {
                    *acc.entry((col, lr)).or_insert(0) += 1;
                }
            }
            if lr_bounds != r_bounds {
                for row in lr_bounds..=r_bounds {
                    *acc.entry((lc, row)).or_insert(0) += 1;
                }
            }
        }
    }
    Ok(acc.iter().filter(|(_, x)| **x >= 2).count())
}


fn get_all_diag_points(a: (i32, i32), b: (i32, i32), acc: &mut HashMap<(i32, i32), i32>) {
    let mut start = a;
    let mut end = b;
    if a.0 < b.0 && a.1 < b.1 {
        *acc.entry(start).or_insert(0) += 1;
        while start != end {
            start.0 += 1;
            start.1 += 1;
            *acc.entry(start).or_insert(0) += 1;
        }
    } else if a.0 > b.0 && a.1 < b.1 {
        *acc.entry(start).or_insert(0) += 1;
        while start != end {
            start.0 -= 1;
            start.1 += 1;
            *acc.entry(start).or_insert(0) += 1;
        }
    } else {
        return get_all_diag_points(b, a, acc);
    }
}

fn print_acc(acc: HashMap<(i32, i32), i32>) {
    let mut buf = String::new();
    for x in 0..10 {
        for y in 0..10 {
            if acc.contains_key(&(y, x)) {
                buf.push_str(format!("{}", acc.get(&(y, x)).unwrap()).as_str());
            } else {
                buf.push_str(".");
            }
        }
        buf.push_str("\n");
    }
    println!("{}", buf);
}
