use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

const CAP: usize = 12;

pub(crate) fn run(path: &str, start: &str, end: &str) -> Result<i64, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let mut adjancencies = vec![false;CAP*CAP];
    let mut graph = Vec::new();
    for line in file.lines() {
        let line_unwrap = line.unwrap();
        let raw = line_unwrap.split("-").collect::<Vec<&str>>();
        let l = String::from(*raw.get(0).unwrap());
        let r = String::from(*raw.get(1).unwrap());

        let li = gidx(&mut graph, l);
        let ri = gidx(&mut graph, r);
        adjancencies[li+ri*CAP] = true;
        adjancencies[ri+li*CAP] = true;
    }

    let mut count = 0;
    let src = gidx(&mut graph, start.into());
    let dst = gidx(&mut graph, end.into());

    let mut visited = vec![false; graph.len()];
    visited[src] = true;
    walk(&graph, &adjancencies, src, src, dst, false, &mut count, &mut visited);

    Ok(count)
}

fn walk(graph: &Vec<String>, adjacencies: &Vec<bool>, start: usize, src: usize, dst: usize, second: bool, count: &mut i64, visited: &mut Vec<bool>) {
    if src == dst {
        *count += 1;
        return;
    }
    for (i, n) in graph.iter().enumerate() {
        if adjacencies[src+i*CAP] {
            if n.chars().next().unwrap().is_uppercase() {
                walk(graph, adjacencies, start, i, dst, second, count, visited);
            } else if !visited[i] {
                visited[i] = true;
                walk(graph, adjacencies, start, i, dst, second, count, visited);
                visited[i] = false;
            } else if !second && i != start {
                walk(graph, adjacencies, start, i, dst, true, count, visited);
            }
        }
    }
}

fn gidx(graph: &mut Vec<String>, label: String) -> usize {
    for (i, l) in graph.iter().enumerate() {
        if *l == label {
            return i;
        }
    }
    graph.push(label);
    graph.len() - 1
}