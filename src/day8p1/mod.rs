use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;


pub(crate) fn run(path: &str) -> Result<usize, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let mut sum = 0;
    for lw in file.lines() {
        let line = lw.unwrap();
        let numbers = line.split("|").last().unwrap().split_whitespace();
        sum += numbers.
            filter(|x| x.len() == 2 || x.len() == 4 || x.len() == 3 || x.len() == 7).count();
    }

    Ok(sum)
}
