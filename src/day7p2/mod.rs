use std::borrow::BorrowMut;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

pub(crate) fn run(path: &str) -> Result<i64, Box<dyn Error>> {
    let file = BufReader::new(File::open(path)?);

    for lw in file.lines() {
        let line = lw?;

        let positions = line.
            split(",").map(|x| x.parse::<i64>().unwrap()).collect::<Vec<i64>>();

        let fuel_spent_map = positions.iter().fold(Vec::new(), |mut acc, x| {
            acc.push(find_diff_linear(positions.clone(), *x));
            acc
        });

        let least_spent = fuel_spent_map.iter().min().ok_or("Unexpected fuel map")?;
        return Ok(*least_spent);
    }
    Ok(0)
}

fn find_diff_linear(positions: Vec<i64>, pos: i64) -> i64 {
    positions.iter().fold(0, |mut acc, x| {
        let n = (*x - pos).abs() as f64;
        acc += ((n / 2.0) * (n + 1.0)) as i64;
        acc
    })
}
