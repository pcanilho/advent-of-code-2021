use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;
use std::iter::FromIterator;

pub(crate) fn run(path: &str) -> Result<u32, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);

    let delim_map: HashMap<char, char> = HashMap::from_iter([
        ('}', '{'), (']', '['), (')', '('), ('>', '<')]);
    let score_map: HashMap<char, u32> = HashMap::from_iter([
        (')', 3), (']', 57), ('}', 1197), ('>', 25137)]);

    let mut score = 0;
    'lw: for line in file.lines() {
        let mut t = line.unwrap();
        let mut stack = Vec::new();
        for c in t.chars() {
            if delim_map.contains_key(&c) {
                if stack.pop().unwrap() != delim_map[&c] {
                    score += score_map[&c];
                    continue 'lw;
                }
            } else {
                stack.push(c);
            }
        }
    }
    Ok(score)
}