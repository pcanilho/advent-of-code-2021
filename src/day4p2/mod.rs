use std::collections::HashMap;
use std::io::{BufReader, Lines};
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;
use std::fmt::{Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::process::exit;
use linked_hash_map::LinkedHashMap;

#[derive(Clone)]
struct Entry {
    row: usize,
    column: usize,
    value: i32,
}

impl Entry {
    fn new(row: usize, column: usize, value: i32) -> Self {
        Entry {
            row,
            column,
            value,
        }
    }
}

impl Debug for Entry {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl Eq for Entry {}

impl PartialEq for Entry {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value
    }
}

#[derive(Debug, Clone)]
struct Board {
    numbers: Vec<(Entry, bool)>,
    size: usize,
    won: bool,
}

impl Board {
    fn new(lines: Vec<String>) -> Self {
        // let mut lines = Vec::new();
        let mut numbers = Vec::new();
        for (i, line) in lines.iter().enumerate() {
            let split: Vec<&str> = line.split_whitespace().collect::<Vec<&str>>();
            let parsed: Vec<i32> = split.iter().map(|x| x.parse::<i32>().unwrap()).collect();
            for (c, p) in parsed.iter().enumerate() {
                numbers.push((Entry::new(i, c, *p), false));
            }
        }
        Board {
            numbers,
            size: lines.len(),
            won: false,
        }
    }

    fn mark(&mut self, v: i32) {
        for (i, (e, m)) in self.numbers.clone().iter().enumerate() {
            if e.value == v {
                self.numbers[i] = (e.clone(), true);
            }
        }
    }

    fn won(&self) -> (bool, i32) {
        let mut marked_rows: HashMap<usize, usize> = HashMap::new();
        let mut marked_columns: HashMap<usize, usize> = HashMap::new();
        let mut unmarked = Vec::new();
        for (e, m) in self.numbers.clone() {
            if !m {
                unmarked.push(e.value);
                continue;
            }
            *marked_rows.entry(e.row).or_insert(0) += 1;
            *marked_columns.entry(e.column).or_insert(0) += 1;
        }
        let unmarked_sum = unmarked.iter().sum();
        for (_, v) in marked_rows {
            if v >= self.size {
                return (true, unmarked_sum);
            }
        }
        for (_, v) in marked_columns {
            if v >= self.size {
                return (true, unmarked_sum);
            }
        }
        (false, -1)
    }
}

pub(crate) fn run(path: &str) -> Result<i32, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let size = 5;
    let mut input = String::new();
    let mut counter = Vec::new();
    let mut boards = Vec::new();
    for (i, lw) in f.lines().enumerate() {
        let line = lw.unwrap();
        if counter.len() == size {
            boards.push(Board::new(counter.clone()));
            counter.clear();
            continue;
        }

        if line.len() == 0 {
            continue;
        }

        if i == 0 {
            input = line;
            continue;
        }

        counter.push(line);
    }

    boards.push(Board::new(counter.clone()));
    let input_raw = input.split(',').map(|x| x.parse::<i32>().unwrap()).collect::<Vec<i32>>();
    let mut boards_won = LinkedHashMap::new();
    for i in input_raw {
        boards.iter_mut().for_each(|x| x.mark(i));
        for (bi, b) in boards.clone().iter().enumerate() {
            let (won, sum) = b.won();
            if won && !boards_won.contains_key(&bi) {
                boards_won.insert(bi, sum*i);
            }
        }
    }
    Ok(*boards_won.values().last().unwrap())
}
