use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;

pub(crate) fn run(path: &str) -> Result<usize, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let mut previous: i32 = 0;
    Ok(f.
        lines().
        map(|x| x.unwrap().parse::<i32>().unwrap()).
        collect::<Vec<i32>>().
        windows(3).
        map(|x| x.iter().sum()).
        collect::<Vec<i32>>().
        iter().
        filter(|x| {
            if previous != 0 {
                if **x > previous {
                    previous = **x;
                    return true;
                }
            }
            previous = **x;
            return false;
        }).
        count())
}
