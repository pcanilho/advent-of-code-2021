use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

const DIRECTIONS: &'static [(i32, i32)] = &[
    (-1, -1), (0, -1), (1, -1),
    (-1, 0), (1, 0),
    (-1, 1), (0, 1), (1, 1)
];

fn broadcast(oct: &mut Vec<u32>, flashed: &mut Vec<bool>, o_rc: (i32, i32), bounds: (i32, i32)) {
    let i_p = (o_rc.1 + o_rc.0 * bounds.0) as usize;
    if flashed[i_p] { return; }

    let c = oct[i_p];
    if c < 9 {
        oct[i_p] += 1;
    } else {
        flashed[i_p] = true;
        oct[i_p] = 0;
        DIRECTIONS.iter().for_each(|(xx, yy)| {
            let pos = (o_rc.0 + xx, o_rc.1 + yy);
            if pos.0 >= 0 && pos.0 < bounds.0 {
                if pos.1 >= 0 && pos.1 < bounds.1 {
                    broadcast(oct, flashed, pos, bounds);
                }
            }
        });
    }
}

pub(crate) fn run(path: &str) -> Result<usize, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let mut oct = Vec::new();
    let (mut row_count, mut col_count) = (0, 0);
    file.lines().for_each(|x| {
        let t = x.unwrap();
        if col_count == 0 { col_count = t.len() as i32; }
        row_count += 1;
        t.chars().for_each(|c| {
            oct.push(c.to_digit(10).unwrap());
        });
    });

    let mut d = 0;
    loop {
        d += 1;
        let mut flashed = vec![false; (row_count * col_count) as usize];
        (0..row_count).for_each(|y: i32| {
            (0..col_count).for_each(|x: i32| {
                broadcast(&mut oct, &mut flashed, (x, y), (row_count, col_count));
            });
        });
        if oct.iter().all(|x| *x == 0) {
            return Ok(d);
        }
    }
}