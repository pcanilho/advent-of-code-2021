use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;


pub(crate) fn run(path: &str) -> Result<usize, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let mut previous: i32 = 0;
    Ok(f.lines().into_iter().filter(|x| {
        let current = x.as_ref().unwrap().parse::<i32>().unwrap();
        if previous != 0 {
            if current > previous {
                previous = current;
                return true
            }
        }
        previous = current;
        return false
    }).count())
}
