use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

const CARDINAL_POINTS: &'static [(i32, i32)] = &[
    (1, 0),     // W
    (-1, 0),    // E
    (0, 1),     // S
    (0, -1)     // N
];

fn lowest(o_row: i32, o_col: i32, bounds: (i32, i32), heightmap: &[u32]) -> bool {
    let (mut n_row, mut n_col): (i32, i32) = (0, 0);
    let initial = heightmap[(o_col + o_row * bounds.1) as usize];
    for p in CARDINAL_POINTS {
        n_row = o_row + p.0;
        n_col = o_col + p.1;
        if n_row >= 0 && n_row < bounds.0 {
            if n_col >= 0 && n_col < bounds.1 {
                let neighbour = heightmap[(n_col + n_row * bounds.1) as usize];
                if neighbour <= initial {
                    return false;
                }
            }
        }
    }
    true
}

pub(crate) fn run(path: &str) -> Result<i32, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let (mut rows_count, mut cols_count): (i32, i32) = (0, 0);
    let mut flatten_heightmap: Vec<u32> = Vec::new();

    file.lines().for_each(|x| {
        rows_count += 1;
        let t = x.unwrap();
        if cols_count == 0 { cols_count = t.len() as i32; }
        t.chars().for_each(|x| {
            flatten_heightmap.push(x.to_digit(10).unwrap());
        });
    });

    let mut basins = Vec::new();
    let mut visited = vec![false; (rows_count * cols_count) as usize];
    (0..rows_count).for_each(|row| {
        (0..cols_count).for_each(|col| {
            if lowest(row, col, (rows_count, cols_count), &flatten_heightmap) {
                let mut basin_size = 0;
                broadcast(row, col,
                          (rows_count, cols_count),
                          &flatten_heightmap,
                          &mut basin_size, &mut visited);
                basins.push(basin_size);
            }
        });
    });

    basins.sort_by(|a, b| b.cmp(a));
    Ok(basins[0]*basins[1]*basins[2])
}

fn broadcast(o_row: i32, o_col: i32, bounds: (i32, i32), heightmap: &Vec<u32>, size: &mut i32, visited: &mut Vec<bool>) {
    let i_pos = (o_col + o_row * bounds.1) as usize;
    if visited[i_pos] { return; }
    *size += 1;
    visited[i_pos] = true;
    let (mut n_row, mut n_col): (i32, i32) = (0, 0);
    let initial = heightmap[i_pos];
    for p in CARDINAL_POINTS {
        n_row = o_row + p.0;
        n_col = o_col + p.1;
        if n_row >= 0 && n_row < bounds.0 {
            if n_col >= 0 && n_col < bounds.1 {
                let neighbour = heightmap[(n_col + n_row * bounds.1) as usize];
                if neighbour >= initial && neighbour < 9 {
                    broadcast(n_row, n_col, bounds, heightmap, size, visited);
                }
            }
        }
    }
}