use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;

pub(crate) fn run(path: &str) -> Result<i32, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let (mut h, mut d, mut a) = (0, 0, 0);

    for line in f.lines() {
        let line_unwrap = line.unwrap();
        let command = line_unwrap.split_whitespace().collect::<Vec<&str>>();
        let direction = command.first().unwrap().as_ref();
        let distance = command.last().unwrap().parse::<i32>()?;

        match direction {
            "up" => a -= distance,
            "down" => a += distance,
            "forward" => {
                h += distance;
                d += a * distance;
            }
            _ => {}
        }
    }
    Ok(h * d)
}
