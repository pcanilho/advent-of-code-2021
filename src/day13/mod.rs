use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

const CAP: usize = 1400;

pub(crate) fn run(path: &str, f_count: i32) -> Result<usize, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let mut paper = vec![false; CAP * CAP];
    let mut folds: Vec<(usize, bool)> = Vec::new();
    let mut row_count = 0;
    let mut col_count = 0;
    for line in file.lines() {
        let line_unwrap = line.unwrap();
        if line_unwrap.len() == 0 { continue; }
        if line_unwrap.starts_with("fold") {
            let axis = line_unwrap.split("=").collect::<Vec<&str>>().get(1).unwrap().parse::<usize>().unwrap();
            if line_unwrap.contains("y=") {
                folds.push((axis, false));
                continue;
            }
            if line_unwrap.contains("x=") {
                folds.push((axis, true));
                continue;
            }
        }

        let coords = line_unwrap.split(",").map(|x| x.parse::<i32>().unwrap()).collect::<Vec<i32>>();
        let (x, y) = (*coords.first().unwrap() as usize, *coords.last().unwrap() as usize);
        paper[y + x * CAP] = true;
        if y > row_count {
            row_count = y;
        }
        if x > col_count {
            col_count = x;
        }
    }

    for (i, f) in folds.iter().enumerate() {
        if f_count > 0 && i >= f_count as usize {
            break;
        }
        if f.1 {
            fold_x(&mut paper, f.0, col_count);
        } else {
            fold_y(&mut paper, f.0, row_count);
        }
    }
    // if f_count == -1 {
    //     print(&paper, 39, 6);
    // }

    Ok(paper.iter().filter(|x| **x).count())
}

fn fold_y(paper: &mut Vec<bool>, fold: usize, row_count: usize) {
    let mut row = 2;
    for y in fold + 1..=row_count {
        for x in 0..CAP {
            if row > y {
                continue;
            }
            let n1 = paper[(y - row) + x * CAP];
            let n2 = paper[y + x * CAP];
            paper[(y - row) + x * CAP] = n1 || n2;
            paper[y + x * CAP] = false;
        }
        row += 2;
    }
}

fn fold_x(paper: &mut Vec<bool>, fold: usize, col_count: usize) {
    for y in 0..CAP {
        let mut col = 2;
        for x in fold + 1..=col_count {
            if col > x {
                continue;
            }
            let n1 = paper[y + (x - col) * CAP];
            let n2 = paper[y + x * CAP];
            paper[y + (x - col) * CAP] = n1 || n2;
            paper[y + x * CAP] = false;
            col += 2;
        }
    }
}

fn print(paper: &Vec<bool>, max_width: usize, max_height: usize) {
    let mut buf = String::new();
    for y in 0..max_height {
        for x in 0..max_width {
            buf.push_str(if paper[y + x * CAP] { "#" } else { "." });
        }
        buf.push_str("\n");
    }
    println!("{}", buf);
}