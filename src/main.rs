use std::error::Error;
use std::time::Instant;

mod day1p1;
mod day1p2;
mod day2p1;
mod day2p2;
mod day3p1;
mod day3p2;
mod day4p1;
mod day4p2;
mod day5p1;
mod day5p2;
mod day6;
mod day7p1;
mod day7p2;
mod day8p1;
mod day8p2;
mod day9p1;
mod day9p2;
mod day10p1;
mod day10p2;
mod day11p1;
mod day11p2;
mod day12p1;
mod day12p2;
mod day13;

fn main() -> Result<(), Box<dyn Error>> {

    /********** DAY 1 **********/
    let mut input_path = "input/day1";
    println!("{}", "-".repeat(30));
    println!("Day 1 :: Part 1: {}", day1p1::run(input_path)?);
    println!("Day 1 :: Part 2: {}", day1p2::run(input_path)?);

    /********** DAY 2 **********/
    input_path = "input/day2";
    println!("{}", "-".repeat(30));
    println!("Day 2 :: Part 1: {}", day2p1::run(input_path)?);
    println!("Day 2 :: Part 2: {}", day2p2::run(input_path)?);

    /********** DAY 3 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day3";
    println!("Day 3 :: Part 1: {}", day3p1::run(input_path)?);
    println!("Day 3 :: Part 2: {}", day3p2::run(input_path)?);

    /********** DAY 4 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day4";
    println!("Day 4 :: Part 1: {}", day4p1::run(input_path)?);
    println!("Day 4 :: Part 2: {}", day4p2::run(input_path)?);

    /********** DAY 5 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day5";
    println!("Day 5 :: Part 1: {}", day5p1::run(input_path)?);
    println!("Day 5 :: Part 2: {}", day5p2::run(input_path)?);

    /********** DAY 6 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day6";
    println!("Day 6 :: Part 1: {}", day6::run(input_path, 79)?);
    println!("Day 6 :: Part 2: {}", day6::run(input_path, 255)?);

    /********** DAY 7 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day7";
    println!("Day 7 :: Part 1: {}", day7p1::run(input_path)?);
    println!("Day 7 :: Part 2: {}", day7p2::run(input_path)?);

    /********** DAY 8 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day8";
    println!("Day 8 :: Part 1: {}", day8p1::run(input_path)?);
    println!("Day 8 :: Part 2: {}", day8p2::run(input_path)?);

    /********** DAY 9 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day9";
    println!("Day 9 :: Part 1: {}", day9p1::run(input_path)?);
    println!("Day 9 :: Part 2: {}", day9p2::run(input_path)?);

    /********** DAY 10 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day10";
    println!("Day 10 :: Part 1: {}", day10p1::run(input_path)?);
    println!("Day 10 :: Part 2: {}", day10p2::run(input_path)?);

    /********** DAY 11 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day11";
    println!("Day 11 :: Part 1: {}", day11p1::run(input_path, 100)?);
    println!("Day 11 :: Part 2: {}", day11p2::run(input_path)?);

    /********** DAY 12 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day12";
    println!("Day 12 :: Part 1: {}", day12p1::run(input_path, "start", "end")?);
    println!("Day 12 :: Part 2: {}", day12p2::run(input_path, "start", "end")?);

    /********** DAY 13 **********/
    println!("{}", "-".repeat(30));
    input_path = "input/day13";
    println!("Day 13 :: Part 1: {}", day13::run(input_path, 1)?);
    println!("Day 13 :: Part 1: {}", day13::run(input_path, -1)?);

    Ok(())
}
