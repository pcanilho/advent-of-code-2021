use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;

pub(crate) fn run(path: &str) -> Result<isize, Box<dyn Error>> {
    let f = File::open(path)?;
    let f = BufReader::new(f);

    let mut values = Vec::new();
    for lw in f.lines() {
        values.push(lw.unwrap());
    }
    let oxygen_rating = find_rating(values.clone(), true);
    let co2_rating = find_rating(values.clone(), false);
    Ok(oxygen_rating * co2_rating)
}

fn find_rating(values: Vec<String>, highest: bool) -> isize {
    let mut n_values = values.clone();
    let mut first_bits = get_nth_bit(n_values.clone(), 0);
    let mut next = find_common(&first_bits, highest);
    let mut it = 1;
    while next.len() != 1 {
        n_values = n_values.iter().enumerate().filter(|(i, _)| next.contains(i)).map(|(_, x)| x.to_owned()).collect();
        first_bits = get_nth_bit(n_values.clone(), it);
        next = find_common(&first_bits, highest);
        it += 1;
    }
    let selected = n_values[next.first().unwrap().to_owned()].clone();
    return isize::from_str_radix(selected.as_str(), 2).unwrap();
}

fn get_nth_bit(v: Vec<String>, it: usize) -> Vec<char> {
    v.iter().map(|x| x.chars().nth(it).unwrap()).collect()
}

fn find_common(i: &Vec<char>, highest: bool) -> Vec<usize> {
    let (zero, one): (Vec<char>, Vec<char>) = (*i).iter().partition(|x| **x == '0');
    let search: Vec<char>;
    if highest {
        if one.len() >= zero.len() {
            search = one;
        } else {
            search = zero;
        }
    } else {
        if one.len() < zero.len() {
            search = one;
        } else {
            search = zero;
        }
    }
    i.iter().enumerate().fold(Vec::new(), |mut acc, x| {
        if search.contains(x.1) {
            acc.push(x.0)
        }
        acc
    })
}
