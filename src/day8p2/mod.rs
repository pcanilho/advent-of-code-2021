use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;
use std::iter::FromIterator;
use std::str::Chars;
use std::time::Instant;
use itertools::Itertools;
use rayon::prelude::*;

const DIGITS_MASKS: &'static [i32] = &[
    0b1110111, // 0
    0b0100100, // 1
    0b1011101, // 2
    0b1101101, // 3
    0b0101110, // 4
    0b1101011, // 5
    0b1111011, // 6
    0b0100101, // 7
    0b1111111, // 8
    0b1101111, // 9
];

fn decode(pattern: &String, value: String) -> i64 {
    let mut mask = 0;
    for i in 0..value.len() {
        for j in 0..7 {
            if pattern.chars().nth(j as usize) == value.chars().nth(i) {
                mask |= (1 << j);
            }
        }
    }
    for i in 0..DIGITS_MASKS.len() {
        if DIGITS_MASKS[i] == mask {
            return i as i64;
        }
    }

    return -1;
}

pub(crate) fn run(path: &str) -> Result<i64, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let mut sum = 0;
    let possible_permutations = "abcdefg".
        chars().
        permutations(7).
        unique().
        collect::<Vec<Vec<char>>>().
        par_iter().
        map(|x| String::from_iter(x)).
        collect::<Vec<String>>();

    for lw in file.lines() {
        let line = lw.unwrap();
        let mut l_split = line.split("|");
        let wires = l_split.next().unwrap().split_whitespace().collect::<Vec<&str>>();
        let values = l_split.next().unwrap().split_whitespace().collect::<Vec<&str>>();

        let mut matching_pattern = possible_permutations.par_iter().find_any(|x| {
            for w in &wires {
                if decode(x, w.clone().into()) < 0 {
                    return false;
                }
            }
            true
        }).unwrap();

        let mut value = 0;
        for v in values {
            value = value * 10 + decode(matching_pattern, v.clone().into());
        }
        sum += value;
    }
    Ok(sum)
}
