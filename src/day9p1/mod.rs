use std::borrow::{Borrow, BorrowMut};
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;

const CARDINAL_POINTS: &'static [(i32, i32)] = &[
    (1, 0),     // W
    (-1, 0),    // E
    (0, 1),     // S
    (0, -1)     // N
];

fn lowest(o_row: i32, o_col: i32, bounds: (i32, i32), heightmap: &[u32]) -> bool {
    let (mut n_row, mut n_col): (i32, i32) = (0, 0);
    let initial = heightmap[(o_col + o_row * bounds.1) as usize];
    for p in CARDINAL_POINTS {
        n_row = o_row + p.0;
        n_col = o_col + p.1;
        if n_row >= 0 && n_row < bounds.0 {
            if n_col >= 0 && n_col < bounds.1 {
                let neighbour = heightmap[(n_col + n_row * bounds.1) as usize];
                if neighbour <= initial {
                    return false;
                }
            }
        }
    }
    true
}

pub(crate) fn run(path: &str) -> Result<u32, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);
    let (mut rows_count, mut cols_count): (i32, i32) = (0, 0);
    let mut flatten_heightmap: Vec<u32> = Vec::new();

    file.lines().into_iter().for_each(|x| {
        rows_count += 1;
        let t = x.unwrap();
        if cols_count == 0 { cols_count = t.len() as i32; }
        t.chars().for_each(|x| {
            flatten_heightmap.push(x.to_digit(10).unwrap());
        });
    });

    let mut sum = 0;
    (0..rows_count).for_each(|row| {
        (0..cols_count).for_each(|col| {
            if lowest(row, col, (rows_count, cols_count), &flatten_heightmap) {
                sum += flatten_heightmap[(col + row * cols_count) as usize] + 1;
            }
        });
    });

    Ok(sum)
}