use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Lines};
use std::io::prelude::*;
use std::iter::FromIterator;

pub(crate) fn run(path: &str) -> Result<i64, Box<dyn Error>> {
    let mut file = BufReader::new(File::open(path)?);

    let delim_map: HashMap<char, char> = HashMap::from_iter([
        ('}', '{'), (']', '['), (')', '('), ('>', '<')]);
    let score_map: HashMap<char, i64> = HashMap::from_iter([
        ('(', 1), ('[', 2), ('{', 3), ('<', 4)]);

    let mut incomplete = Vec::new();
    'lw: for line in file.lines() {
        let mut t = line.unwrap();
        let mut stack = Vec::new();
        for c in t.chars(){
            if delim_map.contains_key(&c) {
                if stack.pop().unwrap() != delim_map[&c] {
                    continue 'lw;
                }
            } else {
                stack.push(c);
            }
        }
        incomplete.push((t, stack));
    }
    let mut scores = Vec::new();
    for i in incomplete {
        let mut ch = i.1.clone();
        ch.reverse();
        scores.push(ch.iter().fold(0i64, |mut acc, x| {
            acc = acc * 5 + score_map[x];
            acc
        }));
    }

    scores.sort();
    Ok(scores[scores.len() / 2].clone())
}